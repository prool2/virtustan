#echo 
#echo {Prool's tintin++ config\r\nproolhelp - help for this config}
#echo
#alias см look
#alias с n
#alias ю s
#alias з w
#alias в e
#alias вв up
#alias вн down
#alias помощь help
#alias см look
#alias инв inv
#alias э eq
#alias кто who
#alias пить drink
#alias есть eat
#alias взять get
#alias вз get
#alias бр drop
#alias сч score
#alias счет score
#alias карта map
#alias время time
#alias дата date
#alias погода weather
#alias lokal #sess local localhost 4000
#alias вмуд #sess vmud mud.kharkov.org 3000
#alias зеркало #sess zerkalo zerkalo.kharkov.org 4000
#alias былины #sess byliny byliny.kharkov.ru 4000
#alias proolhelp #echo {\r\nProol help\r\n\r\n#read /home/prool/tintin++koi.cmd - load koi profile\r\n#read /home/prool/tintin++.cmd - load UTF-8 profile (default)\r\n}
