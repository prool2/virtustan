Virtustan application
=====================

[Screenshots. Скриншоты](http://blog.mud.kharkov.org/screenshots#virtustan-app)

~~эзотерическое~~ приложение, имеющее отношение
к виртуальному королевству Виртустан http://virtustan.kharkov.org

По сути простейшая игра, некая смесь рогалика
([roguelike](http://lurkmore.to/%D0%A0%D0%BE%D0%B3%D0%B0%D0%BB%D0%B8%D0%BA%D0%B8))
и мадоподобного интерфейса ([MUD](http://lurkmore.to/MUD)), с виртустанским контентом

Сюда же отдельным подкаталогом ~~зачем-то~~ вставлен мой мод мад-клиента tintin++

Приложение протестировано в OS Linux (Ubuntu, Debian, Centos), Windows (cygwin),
FreeBSD 9

Новые версии можно найти здесь: https://github.com/prool/virtustan

Автор: [Пруль](http://prool.kharkov.org), mail proolix собака gmail.com

Список использованной литературы и информационных источников

1. Ленин В.И. Полное собрание сочинений
3. Интернет (via Гугль)
4. Мозг Пруля (содержимое его)
5. Virtustan MUD. Исходники
6. Былины MUD. Исходники
7. Circle MUD. Исходники
8. GURPS. Книга игрока
9. nethack. Игра
10. OS Proolix. Исходники
11. Ubuntu 15.10 manpages
